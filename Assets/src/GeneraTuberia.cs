using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneraTuberia : MonoBehaviour
{
    public GameObject tuberias;
    public float tiempo;
    bool puedeGenerar = true;

    void Update()
    {
        if (puedeGenerar) 
        {
            Instantiate(tuberias, transform.position, transform.rotation);
            StartCoroutine(EsperaTiempo());
        }
          
    }
    
    public IEnumerator EsperaTiempo() 
    {
        puedeGenerar = false;
        yield return new WaitForSeconds(tiempo);
        puedeGenerar = true;
    }
}
